from datetime import datetime

'''Devuelve la fecha actual con 00:00:00 como hora'''
def get_today(): 
    now = datetime.now()
    now_str = now.strftime("%Y-%m-%d")
    return datetime.strptime(now_str, "%Y-%m-%d")

'''Devuelve la fecha enviada con 00:00:00 como hora '''
def clean_hour(date): 
    date_str = date.strftime("%Y-%m-%d")
    return datetime.strptime(date_str, "%Y-%m-%d")

'''parse date string a una fecha con 00:00:00'''
def parse_str_to_date(date_str):
    only_date = date_str[:10]
    return datetime.strptime(only_date, "%Y-%m-%d")

