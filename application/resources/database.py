from application.api import app
from application.resources.read import user, password, host, port, dbname
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker


def create_session_server():
    app.config['SQLALCHEMY_DATABASE_URI'] = "mysql+pymysql://" + \
        user + ":" + password + "@" + host + ":" + port + "/" + dbname

    app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
    
    #app.config['SQLALCHEMY_ECHO'] = True

    db = SQLAlchemy(app)
    return db


#session engine
engine = create_engine("mysql+pymysql://" + user + ":" +
                       password + "@" + host + ":" + port + "/" + dbname)
db_session = sessionmaker(bind=engine)
