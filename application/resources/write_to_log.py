from pathlib import Path 

def write_to_log(text, log_name): 
    here = Path.cwd()
    log_path = here / 'application' / 'resources' / log_name
    with open(log_path, mode='a+') as log: 
        log.write(text)
