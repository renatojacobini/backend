#from application.api import db
from application.resources.database import db_session
from application.models.user_model import UserModel
from application.models.event_model import EventModel
from application.models.event_user_model import EventUserModel
from application.models.evaluation_model import EvaluationModel
from application.models.person_model import PersonModel
from application.models.permission_model import user_permission as UserPermissionModel


from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound

from datetime import datetime

class UserController():
    @classmethod
    def insert_user(cls, user):
        session = db_session()
        try:
            session.add(user)
            session.commit()
            return jsonify(status="ok", message="User {} inserted".format(user.username))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_user")
        finally:
            session.close()

    @classmethod
    def query_user_by_username(cls, username):
        session = db_session()
        try:
            return session.query(UserModel).filter_by(username=username).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_user_by_person_id(cls, person_id):
        session = db_session()
        try:
            return session.query(UserModel).filter_by(fk_person_id=person_id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_by_id(cls, id):
        session = db_session()
        try:
            return session.query(UserModel).filter_by(id=id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_evaluators_by_submission(cls, submission_id):
        session = db_session()
        try:
            return session.query(UserModel,EvaluationModel).filter(EvaluationModel.submission_id==submission_id).filter(UserModel.id==EvaluationModel.user_id).all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod 
    def update_content(cls, username, username_new, first_name, last_name):
        session = db_session()
        try:
            now = datetime.now()
            current_user = session.query(UserModel).filter_by(username=username).first()
            current_user.username = username_new
            current_user.first_name = first_name
            current_user.last_name = last_name
            current_user.last_modification_date=now
            current_user.last_modified_user_id=current_user.id
            session.commit()
            return jsonify(status="ok", message="field updated")
        except NoResultFound:
            return jsonify(status="error", message="field no encontrado ")
        finally:
            session.close()
 
    @classmethod 
    def update_password(cls, username, password):
        session = db_session()
        try:
            current_user = session.query(UserModel).filter_by(username=username).first()
            current_user.password = password
            session.commit()
            return jsonify(status="ok", message="field updated")
        except NoResultFound:
            return jsonify(status="error", message="field no encontrado ")
        finally:
            session.close()

    @classmethod 
    def update_request_organizer(cls, username):
        session = db_session()
        try:
            current_user = session.query(UserModel).filter_by(username=username).first()
            current_user.request_organizer = 1
            session.commit()
            return jsonify(status="ok", message="field updated")
        except NoResultFound:
            return jsonify(status="error", message="field no encontrado ")
        finally:
            session.close()
    
    @classmethod 
    def query_all_users(cls): 
        session = db_session()
        try:
            return session.query(UserModel, PersonModel).filter(UserModel.fk_person_id == PersonModel.id).all()

        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod 
    def query_all_created_users_in_range(cls, start_date, end_date):
        session = db_session()
        try:
            return session.query(UserModel).\
                filter(start_date <= UserModel.creation_date).\
                filter(end_date >= UserModel.creation_date).\
                all()

        except NoResultFound:
            return None
        finally:
            session.close()
