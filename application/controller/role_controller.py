from application.resources.database import db_session
from application.models.role_model import RoleModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class RoleController():
    @classmethod
    def insert_role(cls, role):
        session = db_session()
        try:
            session.add(role)
            session.commit()
            return jsonify(status="ok", message="role {} inserted".format(role.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_role")
        finally:
            session.close()

    @classmethod 
    def query_role_by_name(cls, name): 
        session = db_session()
        try:
            return session.query(RoleModel).filter_by(name=name).first() 
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_all_roles(cls):
        def to_json(x):
            return {
                "name": x.name
            }
        session = db_session()
        try:
            return jsonify(roles=list(map(lambda x: to_json(x), session.query(RoleModel).all())))
        except NoResultFound:
            return None
        finally:
            session.close()
    
    @classmethod 
    def insert_all_roles(cls): 
        if not cls.query_role_by_name("Presidente del comité organizador"):
            cls.insert_role(
                RoleModel(name="Presidente del comité organizador"))
        if not cls.query_role_by_name("Miembro del comité organizador"):
            cls.insert_role(
                RoleModel(name="Miembro del comité organizador"))
        if not cls.query_role_by_name("Presidente del comité académico"):
            cls.insert_role(
                RoleModel(name="Presidente del comité académico"))
        if not cls.query_role_by_name("Miembro del comité académico"):
            cls.insert_role(
                RoleModel(name="Miembro del comité académico"))
        if not cls.query_role_by_name("Postulante"):
            cls.insert_role(
                RoleModel(name="Postulante"))
        if not cls.query_role_by_name("Asistente"):
            cls.insert_role(
                RoleModel(name="Asistente"))


