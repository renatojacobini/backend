from application.resources.database import db_session
from application.models.event_user_model import EventUserModel
from application.models.event_model import EventModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class EventUserController():
    @classmethod
    def insert_event_user(cls, event_user):
        session = db_session()
        try:
            session.add(event_user)
            session.commit()
            return jsonify(status="ok", message="event_user {} inserted".format(event_user.id))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert event_user")
        finally:
            session.close()

    @classmethod
    def query_by_event_id_and_user_id(cls, event_id, user_id):
        session = db_session()
        try:
            return session.query(EventUserModel).filter(EventUserModel.user_id==user_id).filter(EventUserModel.event_id==event_id).first()
        except NoResultFound:
            return None
        finally:
            session.close()


    @classmethod
    def query_events_by_user_id(cls, user_id):
        session = db_session()
        try:
            return session.query(EventUserModel, EventModel).\
                join(EventModel).\
                filter(EventUserModel.user_id==user_id).\
                group_by(EventUserModel.event_id)
        
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def find_by_ids(cls, user_id, event_id):
        session = db_session()
        try:
            return session.query(EventUserModel).filter_by(user_id=user_id, event_id=event_id).all()
        except NoResultFound:
            return None
        finally:
            session.close()
            

    @classmethod 
    def update_state(cls, user_id, event_id, result): 
        session = db_session()
        try:
            event_user = session.query(EventUserModel).filter_by(user_id=user_id, event_id=event_id).first()
            if result == 0: 
                event_user.status = 0 
            else: 
                event_user.status = 0
            session.commit()
        except NoResultFound:
            return None
        finally:
            session.close()
