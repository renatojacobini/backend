from application.api import db
from application.resources.database import db_session
from application.models.person_model import PersonModel
from application.models.user_model import UserModel
from application.models.event_user_model import EventUserModel
from application.models.role_model import RoleModel
from application.models.submission_model import submission_persons as SubmissionPersonModel
from application.controller.user_controller import UserController
from application.controller.preference_controller import PreferenceModel
from application.controller.submission_controller import SubmissionModel
from application.models.evaluation_model import EvaluationModel
from flask import jsonify
from sqlalchemy import text
from sqlalchemy.orm.exc import NoResultFound


class PersonController():
    @classmethod
    def insert_person(cls, person):
        session = db_session()
        try:
            session.add(person)
            session.commit()
            return jsonify(status="ok", message="Person {} inserted".format(person.email))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_person")
        finally:
            session.close()

    @classmethod
    def query_person_by_email(cls, email):
        session = db_session()
        try:
            return session.query(PersonModel).filter_by(email=email).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_by_id(cls, id):
        session = db_session()
        try:
            return session.query(PersonModel).filter_by(id=id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_by_submission(cls, submission_id):
        session = db_session()
        try:
            return session.query(SubmissionPersonModel).filter_by(submission_id=submission_id).all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_person_by_first_name(cls, first_name):
        session = db_session()
        try:
            return session.query(PersonModel).filter_by(first_name=first_name).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_person_by_last_name(cls, last_name):
        session = db_session()
        try:
            return session.query(PersonModel).filter_by(first_name=last_name).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_person_by_user_id(cls, user_id):
        session = db_session()
        try:
            return session.query(PersonModel).\
            filter(UserModel.id==user_id).\
            filter(UserModel.fk_person_id==PersonModel.id).\
            first()
        except NoResultFound:
            return None
        finally:
            session.close()    

    @classmethod
    def query_evaluators_by_submission(cls, submission_id):
        session = db_session()
        try:
            return session.query(PersonModel, UserModel).\
                    filter(EvaluationModel.submission_id==submission_id).\
                    filter(UserModel.id==EvaluationModel.user_id).\
                    filter(PersonModel.id==UserModel.fk_person_id).\
                    all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_evaluators_by_event(cls, event_id):
        session = db_session()
        try:
            return session.query(PersonModel, UserModel,EventUserModel).\
                    filter(EventUserModel.event_id==event_id).\
                    filter(UserModel.id==EventUserModel.user_id).\
                    filter(PersonModel.id==UserModel.fk_person_id).\
                    filter(EventUserModel.event_user_role.any(name='Miembro del comité académico')).\
                    all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_available_evaluators_by_event(cls, event_id,submission_id):
        session = db_session()
        try:
            assignated = session.query(UserModel.id).\
                        filter(SubmissionModel.id==submission_id).\
                        filter(EvaluationModel.submission_id==SubmissionModel.id).\
                        filter(EvaluationModel.user_id==UserModel.id).\
                        all()
            subquery = []
            for user in assignated:
                subquery.append(user.id)

            return session.query(PersonModel, UserModel,EventUserModel).\
                    filter(EventUserModel.event_id==event_id).\
                    filter(UserModel.id==EventUserModel.user_id).\
                    filter(PersonModel.id==UserModel.fk_person_id).\
                    filter(EventUserModel.event_user_role.any(name='Miembro del comité académico')).\
                    filter(UserModel.id.notin_(subquery)).\
                    all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_authors_by_submission(cls, submission_id):
        session = db_session()
        try:
            return session.query(PersonModel, UserModel).\
                    filter(SubmissionModel.id==submission_id).\
                    filter(EvaluationModel.submission_id==SubmissionModel.id).\
                    filter(EvaluationModel.user_id==UserModel.id).\
                    filter(PersonModel.id==UserModel.fk_person_id).\
                    all()
                    

        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_members_by_event(cls, event_id):
        session = db_session()
        try:
            query = text('''
                select p.email as email, r.name as role
                from person p
                inner join user u on p.id=u.fk_person_id
                inner join event_user eu on eu.user_id=u.id
                inner join event_user_role eur on eur.event_user_id=eu.id
                inner join role r on r.id=eur.role_id
                where eu.event_id='''+str(event_id))
            
            return  db.engine.execute(query)

            #session.query(PersonModel).\
            #filter(PersonModel.id==UserModel.fk_person_id).\
            #filter(UserModel.id==EventUserModel.user_id).\
            #filter(EventUserModel.event_id==event_id).\
            #all()
            #filter(RoleModel.event_user_roles.any(id=EventUserModel.id)).\
            #filter(EventUserModel.event_user_role.any(id=id)).\
            #join(RoleModel).\
                    
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_authors_by_submission(cls, submission_id):
        session = db_session()
        try:
            return session.query(PersonModel).\
                    filter(PersonModel.submission_persons.any(id=submission_id)).\
                    all()
        
        except NoResultFound:
            return None
        finally:
            session.close()



