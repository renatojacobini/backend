from application.resources.database import db_session
from application.models.type_model import TypeModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class TypeController():
    @classmethod
    def insert_type(cls, type):
        session = db_session()
        try:
            session.add(type)
            session.commit()
            return jsonify(status="ok", message="type {} inserted".format(type.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_type")
        finally:
            session.close()

    @classmethod 
    def query_type_by_name(cls, name): 
        session = db_session()
        try:
            return session.query(TypeModel).filter_by(name=name).first() 
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod 
    def query_by_id(cls, id): 
        session = db_session()
        try:
            return session.query(TypeModel).filter_by(id=id).first() 
        except NoResultFound:
            return None
        finally:
            session.close()


    @classmethod 
    def query_all_types(cls): 

        def to_json(x): 
            return {
                "name": x.name
            }
        session = db_session()
        try:
            return jsonify(types= list(map(lambda x: to_json(x), session.query(TypeModel).all())))
        except NoResultFound:
            return None
        finally:
            session.close()
    
    @classmethod
    def insert_all_types(cls): 
        if not cls.query_type_by_name("Taller"):
            cls.insert_type(TypeModel(name="Taller"))
        if not cls.query_type_by_name("Seminario"): 
            cls.insert_type(TypeModel(name="Seminario"))
        if not cls.query_type_by_name("Congreso"): 
            cls.insert_type(TypeModel(name="Congreso"))
        if not cls.query_type_by_name("Conferencia"): 
            cls.insert_type(TypeModel(name="Conferencia"))
        if not cls.query_type_by_name("Simposio"): 
            cls.insert_type(TypeModel(name="Simposio"))
        if not cls.query_type_by_name("Diplomado"):
            cls.insert_type(TypeModel(name="Diplomado"))
            
    

