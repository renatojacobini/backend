from application.resources.database import db_session
from application.models.keyword_model import KeywordModel
from application.models.event_model import EventModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class KeywordController():
    @classmethod
    def insert_keyword(cls, keyword):
        session = db_session()
        try:
            session.add(keyword)
            session.commit()
            return jsonify(status="ok", message="keyword {} inserted".format(keyword.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_keyword")
        finally:
            session.close()

    @classmethod
    def query_keyword_by_name(cls, name):
        session = db_session()
        try:
            return session.query(KeywordModel).filter_by(name=name).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_keyword_by_id(cls, id):
        session = db_session()
        try:
            return session.query(KeywordModel).filter_by(id=id).first()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_keywords_by_submission(cls, submission_id):
        session = db_session()
        try:
            return session.query(KeywordModel).filter(KeywordModel.submission_keywords.any(id=submission_id)).all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_keywords_by_event(cls, event_id):
        session = db_session()
        try:
            return session.query(KeywordModel).filter(KeywordModel.event_keyword.any(id=event_id)).all()
        except NoResultFound:
            return None
        finally:
            session.close()


    @classmethod
    def query_by_evaluator(cls, evaluator_id):
        session = db_session()
        try:
            return session.query(KeywordModel).filter(KeywordModel.event_user_keyword.any(id=evaluator_id)).all()
        except NoResultFound:
            return None
        finally:
            session.close()

    @classmethod
    def query_all_keywords(cls):

        def to_json(x):
            return {
                "name": x.name
            }
        session = db_session()
        try:
            return jsonify(types=list(map(lambda x: to_json(x), session.query(KeywordModel).all())))
        except NoResultFound:
            return None
        finally:
            session.close()
			
			
    @classmethod
    def query_keywords_by_event(cls, event_id):
        session = db_session()
        try:
            return session.query(KeywordModel).filter(KeywordModel.event_keyword.any(id=event_id)).all()
            #return session.query(KeywordModel.name).join(EventModel).EventModel. == EventUserRoleModel.c.event_user_id.all()
            # return session.query(EventUserModel).filter_by(user_id=user_id)
        except NoResultFound:
            return None
        finally:
            session.close()
