from application.resources.database import db_session
from application.models.criterium_model import CriteriumModel
from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class CriteriumController():
    @classmethod
    def insert_criterium(cls, criterium):
        session = db_session()
        try:
            session.add(criterium)
            session.commit()
            return jsonify(status="ok", message="criterium {} inserted".format(criterium.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in insert_criterium")
        finally:
            session.close()

    @classmethod
    def delete_criterium(cls, criterium):
        session = db_session()
        try:
            session.delete(criterium)
            session.commit()
            return jsonify(status="ok", message="criterium {} deleted".format(criterium.name))
        except:
            session.rollback()
            return jsonify(status="error", message="error in delete_criterium")
        finally:
            session.close()

    @classmethod
    def query_by_phase_id(cls, phase_id):
        session = db_session()
        try:
            return session.query(CriteriumModel).filter(CriteriumModel.phase_id==phase_id).all()
        except NoResultFound:
            return None
        finally:
            session.close()
