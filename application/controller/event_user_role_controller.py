from application.resources.database import db_session
from application.models.event_user_model import EventUserModel
from application.models.role_model import RoleModel
from application.models.role_model import event_user_role as EventUserRoleModel
from application.controller.event_user_controller import EventUserController

from flask import jsonify
from sqlalchemy.orm.exc import NoResultFound


class EventUserRoleController():

    @classmethod
    def query_roles_by_event_user_id(cls, event_id,user_id):
        session = db_session()
        try:
            return session.query(RoleModel.name).\
                filter(EventUserModel.event_id == event_id).\
                filter(EventUserModel.user_id == user_id).\
                filter(EventUserModel.id == EventUserRoleModel.c.event_user_id).\
                filter(EventUserRoleModel.c.role_id == RoleModel.id).all()
            # return session.query(EventUserModel).filter_by(user_id=user_id)
        except NoResultFound:
            return None
        finally:
            session.close()
