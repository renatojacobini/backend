from application.api import db

class CriteriumModel(db.Model): 
    __tablename__ = 'criterium' 

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120), nullable = False)
    phase_id = db.Column(db.Integer, db.ForeignKey('phase.id'))
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)

