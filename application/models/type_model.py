from application.api import db


class TypeModel(db.Model): 
    __tablename__ = 'type' 

    id = db.Column(db.Integer, primary_key = True)
    name = db.Column(db.String(120), nullable = False, unique= True)
    event = db.relationship('EventModel', backref='type')

    def __repr__(self):
        return "<Tipo id='%s'>" % (self.id)
