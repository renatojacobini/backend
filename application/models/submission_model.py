from application.api import db

#autores
submission_persons = db.Table('submission_persons',
    db.Column('submission_id', db.Integer, db.ForeignKey('submission.id'), primary_key=True),
    db.Column('person_id', db.Integer, db.ForeignKey('person.id'), primary_key=True)
)

#keywords
submission_keywords = db.Table('submission_keywords',
    db.Column('submission_id', db.Integer, db.ForeignKey('submission.id'), primary_key=True),
    db.Column('keyword_id', db.Integer, db.ForeignKey('keyword.id'), primary_key=True)
)


class SubmissionModel(db.Model): 
    __tablename__ = 'submission' 

    id = db.Column(db.Integer, primary_key = True)
    status = db.Column(db.Integer, default=1)
    name = db.Column(db.String(120), nullable = False)
    document = db.Column(db.String(250), nullable = True)
    summary = db.Column(db.String(500), nullable = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    phase_id = db.Column(db.Integer, db.ForeignKey('phase.id'))
    president_result = db.Column(db.Integer, nullable = True)
    president_comment = db.Column(db.String(120), nullable = True)
    evaluation = db.relationship('EvaluationModel', backref='submission')
    authors = db.relationship('PersonModel', secondary=submission_persons, backref=db.backref('submission_persons',lazy='dynamic'))
    keywords = db.relationship('KeywordModel', secondary=submission_keywords, backref=db.backref('submission_keywords',lazy='dynamic'))
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)
