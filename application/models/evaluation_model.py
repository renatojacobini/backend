from application.api import db

class EvaluationModel(db.Model): 
    __tablename__ = 'evaluation' 

    id = db.Column(db.Integer, primary_key = True)
    trust = db.Column(db.SmallInteger, nullable = True)
    score = db.Column(db.SmallInteger, nullable = True)
    comment_author = db.Column(db.String(500), nullable = True)
    comment_president = db.Column(db.String(500), nullable = True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    submission_id = db.Column(db.Integer, db.ForeignKey('submission.id'))
    evaluated = db.Column(db.SmallInteger, default=0)
    #Trazabilidad
    creation_date = db.Column(db.DateTime, nullable = True)
    last_modification_date = db.Column(db.DateTime, nullable = True)
    creation_user_id = db.Column(db.Integer, nullable = True)
    last_modified_user_id = db.Column(db.Integer, nullable = True)
