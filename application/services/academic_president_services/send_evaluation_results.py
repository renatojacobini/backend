from flask_restful import Resource, reqparse
from flask import json, jsonify 
import smtplib
import os
import json
from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from application.controller.user_controller import UserController
from application.controller.person_controller import PersonController
from application.controller.submission_controller import SubmissionController
from application.controller.phase_controller import PhaseController
from application.controller.event_controller import EventController
from application.controller.evaluation_controller import EvaluationController
from application.models.evaluation_model import EvaluationModel

parser = reqparse.RequestParser()
parser.add_argument('id', help='Not Blank', required=True)
parser.add_argument('comment', help = 'Not Blank', required=True)
 
SYSTEM_EMAIL = os.environ.get('SYSTEM_EMAIL')
SYSTEM_PASSWORD = os.environ.get('SYSTEM_PASSWORD')

class SendEvaluationResults(Resource):
    def post(self):
        data = parser.parse_args()
        all_guests = []

        current_submission = SubmissionController.query_by_id(data['id'])
        result = current_submission.president_result
        submission_name = current_submission.name
        current_user = UserController.query_by_id(current_submission.user_id)
        current_person = PersonController.query_by_id(current_user.fk_person_id)
        person_name = current_person.first_name
        current_phase = PhaseController.query_phase_by_id(current_submission.phase_id)
        current_event = EventController.query_event_by_id(current_phase.event_id)
        event_name = current_event.name

        all_guests.append(current_person.email)

        all_comments = '<ul>\n'
        evaluations = EvaluationController.query_evaluations_by_submission(data['id'])
        for ev in evaluations:
            all_comments = all_comments + '<li>{}</li>\n'.format(ev.EvaluationModel.comment_author)

        all_comments = all_comments + '\n</ul>'

        if result==0:
            res_msg = "El resultado del entregable fue desaprobatorio, lo cual significa que ya no podra postular en las siguientes fases."
        else:
            res_msg = "El entregable ha sido aprobado por el comite academico."

        messageHTML = '''
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>Rendezvous Mail</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body style="margin: 0; padding: 0;">
	<table border="0" cellpadding="0" cellspacing="0" width="100%">	
		<tr>
			<td style="padding: 10px 0 30px 0;">
				<table align="center" border="0" cellpadding="0" cellspacing="0" width="600" style="border: 1px solid #cccccc; border-collapse: collapse;">
					<tr>
						<td align="left" bgcolor="#003c7d" style="padding: 10px 30px 10px 30px; color: #153643; font-size: 28px; font-weight: bold; font-family: Arial, sans-serif;">
                            <img src="https://i.postimg.cc/KYhQgHmw/Rendezvous.png" alt="Rendezvous" width="200" height="60" style="display: block;" />
                        </td>
					</td>
					<tr>
						<td bgcolor="#ffffff" style="padding: 40px 30px 40px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td align="center" style="color: #153643; font-family: Arial, sans-serif; font-size: 24px;">
										<b>{}</b>
									</td>
								</tr>
								<tr>
									<td style="padding: 20px 0 30px 0; color: #153643; font-family: Arial, sans-serif; font-size: 16px; line-height: 20px;">
										<p>Estimado(a) {},</p>
										<br>Su entregable "{}" ya ha sido evaluado por el comité académico del evento. {}<br/><br/>
										El comentario del presidente del comité académico sobre su entregable fue:</br></br>
										<center>{}</center></br></br>
										Adicinalmente, ha recibido las siguientes observaciones por parte de los evaluadores:</br>
										{}</br>
										</p>
									</td>
								</tr>
								
							</table>
						</td>
					</tr>
					<tr>
						<td bgcolor="#DD0000" style="padding: 30px 30px 30px 30px;">
							<table border="0" cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td style="color: #ffffff; font-family: Arial, sans-serif; font-size: 14px;" width="75%">
										&reg; Rendezvous, Software 2019<br/>
										Todos los derechos reservados.
									</td>
									<td align="right" width="25%">
										<table border="0" cellpadding="0" cellspacing="0">
											<tr>
												<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
													<a href="#" style="color: #ffffff;">
														<img src="https://cdn3.iconfinder.com/data/icons/capsocial-round/500/twitter-512.png" alt="Twitter" width="30" height="30" style="display: block;" border="0" />
													</a>
												</td>
												<td style="font-size: 0; line-height: 0;" width="20">&nbsp;</td>
												<td style="font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">
													<a href="#" style="color: #ffffff;">
														<img src="https://cdn3.iconfinder.com/data/icons/capsocial-round/500/facebook-512.png" alt="Facebook" width="30" height="30" style="display: block;" border="0" />
													</a>
												</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
</body>
</html>
        '''.format(event_name,person_name,submission_name,res_msg,data['comment'],all_comments)
        
        all_guests = list(set(all_guests))
        # CONFIGURACION DEL MENSAJE
        msg = MIMEMultipart('alternative')
        #msg['Subject'] = 'CIENCIAS E INGENIERIA'
        msg['Subject'] = '[RENDEZVOUS] - OBSERVACIONES DE ENTREGABLE'
        msg.attach(MIMEText(messageHTML, 'html'))
        
        with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
            smtp.login(SYSTEM_EMAIL, SYSTEM_PASSWORD)
            smtp.sendmail(SYSTEM_EMAIL, all_guests, msg.as_string())

        return jsonify(status="ok", message="se han enviado todos los correos")
