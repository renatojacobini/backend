from flask_restful import Resource, reqparse
from application.controller.submission_controller import SubmissionController 
from application.controller.user_controller import UserController 
from application.controller.keyword_controller import KeywordController
from application.controller.person_controller import PersonController
from application.controller.phase_controller import PhaseController 
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('phase_id', help='Not Blank', required=True)

class ViewSubmissions(Resource): 
	def post(self):
		data = parser.parse_args()
		submissions = SubmissionController.query_by_phase(data['phase_id'])
		submissions_json=[]
		for submission in submissions:
			author_user = UserController.query_by_id(submission.SubmissionModel.user_id)
			author = PersonController.query_by_id(author_user.fk_person_id)
			
			keywords = KeywordController.query_keywords_by_submission(submission.SubmissionModel.id)
			keywords_json = []
			for keyword in keywords:
				keywords_json.append({"name":keyword.name})

			kws = [{"id":kw.id,"name":kw.name} for kw in keywords]

			if submission.SubmissionModel.president_result is None:
				president_result = "Sin veredicto"
			elif submission.SubmissionModel.president_result == 0:
				president_result = "Rechazado"
			else: 
				president_result = "Aceptado"
			dicc = {
				"id":submission.SubmissionModel.id,
				"name":submission.SubmissionModel.name,
				"author": author.first_name+" "+author.last_name,
				"summary":submission.SubmissionModel.summary,
				"keywords":keywords_json,
				"president_result": president_result
			 }
			submissions_json.append(dicc)
		phase = PhaseController.query_phase_by_id(data['phase_id'])

		return jsonify(phase=phase.name,start_date=phase.start_date,end_date=phase.end_date,submissions=submissions_json)


