from flask_restful import Resource, reqparse
from application.controller.keyword_controller import KeywordController 
from application.controller.person_controller import PersonController 
from application.controller.submission_controller import SubmissionController 
from application.controller.evaluation_controller import EvaluationController 
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('id_submission', help='Not Blank', required=True)

class ViewDataEvaluationPresident(Resource): 
    def post(self):
        data = parser.parse_args()
        submission = SubmissionController.query_by_id(data['id_submission'])
        return jsonify(
            president_result=submission.president_result,
            president_comment=submission.president_comment
            )
