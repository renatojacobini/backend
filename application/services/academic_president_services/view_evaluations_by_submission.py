from flask_restful import Resource, reqparse
from application.controller.evaluation_controller import EvaluationController
from application.controller.criterium_evaluation_controller import CriteriumEvaluationController
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('submission_id', help='Not Blank', required=True)

class ViewEvaluationsBySubmission(Resource): 
	def post(self):
		data = parser.parse_args()
		evaluations = EvaluationController.query_evaluations_by_submission(data['submission_id'])
		evaluations_json=[]
		for evaluation in evaluations:
			qualification_json = []
			qualifications = CriteriumEvaluationController.query_by_evaluation(evaluation.EvaluationModel.id)
			for qualification in qualifications:
				qualification_json.append({
											"idCriterio": qualification.CriteriumModel.id,
											"criterio" : qualification.CriteriumModel.name,
											"score":qualification.CriteriumEvaluationModel.score
											})
			dicc = {
				"evaluator":evaluation.PersonModel.first_name+" "+evaluation.PersonModel.last_name,
				"trust":evaluation.EvaluationModel.trust,
				"score":evaluation.EvaluationModel.score,
				"comment_author":evaluation.EvaluationModel.comment_author,
				"comment_president":evaluation.EvaluationModel.comment_president,
				"qualification":qualification_json
				}
			evaluations_json.append(dicc)
 
		return jsonify(evaluations=evaluations_json)