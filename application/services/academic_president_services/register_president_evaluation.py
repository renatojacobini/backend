from flask_restful import Resource, reqparse
from application.controller.submission_controller import SubmissionController
from application.controller.event_user_controller import EventUserController
from application.controller.user_controller import UserController
from flask import jsonify, json
from application.resources.write_to_log import write_to_log
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('submission_id', help='Not Blank', required=True)
parser.add_argument('president_result', help='Not Blank', required=True)
parser.add_argument('president_comment', help='Not Blank', required=True)
parser.add_argument('username', help='Not Blank', required=False)
parser.add_argument('event_id', help='Not Blank', required=True)
class RegisterPresidentEvaluation(Resource): 
	def post(self):
		data = parser.parse_args()
		now = datetime.now()
		user = UserController.query_user_by_username(data['username'])
		user_id = user.id if user else None
		submission = SubmissionController.query_by_id(data['submission_id'])
		submission.president_result = data['president_result']
		submission.president_comment = data['president_comment']
		submission.last_modification_date=now
		submission.last_modified_user_id=user_id
		SubmissionController.insert_submission(submission)
		# se desliga al usuario del evento 
		EventUserController.update_state(user_id, int(data['event_id']), int(data['president_result']))

		write_to_log(datetime.now().strftime("%d-%m-%Y (%H:%M:%S): ") +
                    "Se registró la evaluación final para el entregable " + data['submission_id'] + "\n", 'log.txt')

		return jsonify(status="ok", message="Se registró correctamente la evaluación")
