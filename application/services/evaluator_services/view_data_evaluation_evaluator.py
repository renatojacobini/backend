from flask_restful import Resource, reqparse
from application.controller.keyword_controller import KeywordController 
from application.controller.person_controller import PersonController 
from application.controller.submission_controller import SubmissionController 
from application.controller.evaluation_controller import EvaluationController 
from application.controller.user_controller import UserController 
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('phase_id', help='Not Blank', required=True)

class ViewDataEvaluationEvaluator(Resource): 
    def post(self):
        data = parser.parse_args()
        current_user = UserController.query_user_by_username(data['username'])
        evaluation = EvaluationController.query_evaluation_by_user_and_phase(current_user.id,data['phase_id'])
        return jsonify(
            trust=evaluation.trust,
            score=evaluation.score,
            comment_author=evaluation.comment_author,
            comment_president=evaluation.comment_president
            )
