from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.phase_controller import PhaseController
from application.controller.keyword_controller import KeywordController
from application.controller.evaluation_controller import EvaluationController
from application.controller.submission_controller import SubmissionController


from flask import jsonify, json


parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('phase_id', help='Not Blank', required=True)

class MyEvaluations(Resource): 
    def post(self):
        data = parser.parse_args()
        print(data['username'] + " " + str(data['phase_id']))
        
        if '@' in str(data['username']):
            current_person = PersonController.query_person_by_email(
                data['username'])
            if not current_person:
                return jsonify(status="error", message="Person with email {} not exist".format(data['username']))
            #se busca el usuario de la persona
            current_user = UserController.query_user_by_person_id(
                current_person.id)
        else:
            current_user = UserController.query_user_by_username(
                data['username'])
            if not current_user:
                return jsonify(status="error", message='Username {} not exist'.format(data['username']))

        current_phase = PhaseController.query_phase_by_id(data['phase_id'])
        if not current_phase:
            return jsonify(status="error", message="Phase doesn't exist")

        evaluations = EvaluationController.query_evaluation_by_user_and_phase(
            current_user.id, current_phase.id)

        evaluation_json = []
        for evaluation in evaluations:
            # submission = SubmissionController.query_by_id(evaluation.EvaluationModel.submission_id)
            # username = UserController.query_by_id(submission.user_id).username
            username = UserController.query_by_id(evaluation.SubmissionModel.user_id).username
            keywords = KeywordController.query_keywords_by_submission(evaluation.SubmissionModel.id)
            keywords_json = []
            for keyword in keywords:
                keywords_json.append(keyword.name)

            evaluation_json.append({
                "id":evaluation.EvaluationModel.id,
                "name":evaluation.SubmissionModel.name,
                "state":evaluation.EvaluationModel.evaluated,
                "user": username,
                "category": keywords_json})


        return jsonify(evaluations=evaluation_json)
