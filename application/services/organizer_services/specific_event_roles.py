from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.type_controller import TypeController

from flask import jsonify, json
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('id_event', help='Not Blank', required=True)


def unprocess_date(date):
    datestr = date.strftime("%d/%m/%Y")
    return datestr

class SpecificEventRoles(Resource): 
    def post(self):
        data = parser.parse_args()
        id_event = data['id_event']
        if '@' in str(data['username']):
            current_person = PersonController.query_person_by_email(
                data['username'])
            if not current_person:
                return jsonify(status="error", message="Person with email {} not exist".format(data['username']))
            #se busca el usuario de la persona
            current_user = UserController.query_user_by_person_id(
                current_person.id)
        else:
            current_user = UserController.query_user_by_username(
                data['username'])
            if not current_user:
                return jsonify(status="error", message='Username {} not exist'.format(data['username']))
        # se obtienen los eventos del usuario
        event = EventController.query_event_by_id(id_event)
        roles_user = EventUserRoleController.query_roles_by_event_user_id(user_id=current_user.id, event_id=id_event)

        # ahora para cada evento se obtienen los roles
        role_json = role_user_json = []
        for role_user in roles_user:
            role_json.append({"name":role_user.name})
		#finalmente se agrega al evento
        type_str = TypeController.query_by_id(event.type_id) 
        return jsonify (
            roles= role_json,
            description= event.description,
            amount= event.amount,
            name= event.name,
            vacancies= event.vacancies,
            startDate=  event.start_date.strftime("%d/%m/%Y"),
            endDate=event.end_date.strftime("%d/%m/%Y"),
            location= event.location,
            programme= event.programme,
            picture= event.picture,
            type= type_str.name
		)

