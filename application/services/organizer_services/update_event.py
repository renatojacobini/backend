from flask_restful import Resource, reqparse
from application.models.type_model import TypeModel
from application.models.keyword_model import KeywordModel
from application.models.event_model import EventModel
from application.models.event_user_model import EventUserModel
from application.models.phase_model import PhaseModel
from application.models.criterium_model import CriteriumModel
from application.models.field_model import FieldModel
from email.message import EmailMessage
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
import smtplib

from flask import jsonify, request
from application.resources.write_to_log import write_to_log
from application.controller.event_controller import EventController
from application.controller.event_user_controller import EventUserController
from application.controller.type_controller import TypeController
from application.controller.keyword_controller import KeywordController
from application.controller.user_controller import UserController
from application.controller.person_controller import PersonController
from application.controller.role_controller import RoleController
from application.controller.phase_controller import PhaseController
from application.controller.field_controller import FieldController
from application.controller.criterium_controller import CriteriumController

from application.resources.aws_s3 import upload_file_to_s3, upload_image_to_s3, delete_file_of_s3

from werkzeug.datastructures import FileStorage
from werkzeug.utils import secure_filename

import json

# HEIL FILESYSTEM
import os
from pathlib import Path
from datetime import datetime

parser = reqparse.RequestParser()
parser.add_argument('event_id', help='Not Blank', required=True)
parser.add_argument('name', help='Not Blank', required=True)
parser.add_argument('description', help='Not Blank', required=True)
parser.add_argument('vacancies', help='Not Blank', required=True)
parser.add_argument('location', help='Not Blank', required=True)
parser.add_argument('start_date', help='Not Blank', required=False)
parser.add_argument('end_date', help='Not Blank', required=False)
parser.add_argument('keywords', help='Not Blank', required=False)
parser.add_argument('type', help='Not Blank', required=True)
parser.add_argument('members', help='Not Blank', required=True)
parser.add_argument('programme', type=FileStorage,
                    help='Not Blank', required=False, location='files')
parser.add_argument('picture', type=FileStorage,
                    help='Not Blank', required=False, location='files')
parser.add_argument('amount', required=False)
parser.add_argument('status', required=False)
parser.add_argument('dayCfP', required=False)
parser.add_argument('username', help='Not Blank', required=False)
parser.add_argument('link', help='Not Blank', required=False)
SYSTEM_EMAIL = os.environ.get('SYSTEM_EMAIL')
SYSTEM_PASSWORD = os.environ.get('SYSTEM_PASSWORD')

#FILES MANAGE
application_folder = Path.cwd().joinpath('application')
papers_folder = application_folder.joinpath('files', 'papers')
images_folder = application_folder.joinpath('files', 'images')

ALLOWED_EXTENSIONS = ['pdf', 'jpg', 'png', 'jpeg', 'txt']


def process_date(date):
    try: 
        int(date[0])
        return datetime.strptime((date.replace("T", " "))[0:19], '%Y-%m-%d %H:%M:%S')
    except ValueError:
        return datetime.strptime(date[0:25], '%a, %d %b %Y %H:%M:%S')



def allowed_file(filename):
    return '.' in filename and filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def send_mail(guest_list,link):
    messageHTML = """<!-- NAME: MEMBER WELCOME -->
		<!--[if gte mso 15]>
		<xml>
			<o:OfficeDocumentSettings>
			<o:AllowPNG></o:AllowPNG>
			<o:PixelsPerInch>96</o:PixelsPerInch>
			</o:OfficeDocumentSettings>
		</xml>
		<![endif]-->
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <!--[if !mso]><!-->
<link href="https://fonts.googleapis.com/css?family=Merriweather:400,400i,700,700i|Merriweather+Sans:400,400i,700,700i|Roboto:400,400i,700,700i" rel="stylesheet"><!--<![endif]-->
        <!--*|IF:MC_PREVIEW_TEXT|*-->
        <!--[if !gte mso 9]><!---->
<span class="mcnPreviewText" style="display:none; font-size:0px; line-height:0px; max-height:0px; max-width:0px; opacity:0; overflow:hidden; visibility:hidden; mso-hide:all;"></span><!--<![endif]-->
        <!--*|END:IF|*-->
<center>
  <table align="center" border="0" cellpadding="0" cellspacing="0" height="100%" width="100%" id="bodyTable" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;background-color: #003c7d;">
    <tbody>
      <tr>
        <td align="center" valign="top" id="bodyCell" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;height: 100%;margin: 0;padding: 0;width: 100%;border-top: 0;"><!-- BEGIN TEMPLATE // -->
          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
            <tbody>
              <tr>
                <td align="center" valign="top" id="templatePreheader" style="background:#ffffff none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 8px solid #003c7d;border-bottom: 0;padding-top: 9px;padding-bottom: 9px;"><!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;">
                    <tbody>
                      <tr>
                        <td valign="top" class="preheaderContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnTextBlockOuter">
                              <tr>
                                <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">

                                  <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 390px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                      <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: left;">
                                          <img data-file-id="4929" height="78" src="https://www.rancard.com/wp-content/uploads/2017/06/rendezvous-logo.jpg" style="border: 0px;width: 270px;height: 78px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="300">
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>

                                  <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 210px;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                      <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-left: 18px;padding-bottom: 9px;padding-right: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #656565;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: left;">
                                          <br>
                                          <div style="text-align: center;">
                                            <span style="font-size:13px">
                                              <span style="font-family:merriweather sans,helvetica neue,helvetica,arial,sans-serif">
                                                <strong>(888) 546-7483
                                                  <br>
                                                  <a href="mailto:rendezvoussystem@gmail.com" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #656565;font-weight: normal;text-decoration: underline;">rendezvous@gmail.com</a>
                                                </strong>
                                              </span>
                                            </span>
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
                </td>
              </tr>
              <tr>
                <td align="center" valign="top" id="templateHeader" style="background:#dff0f4 url(&quot;https://www.lazarski.pl/fileadmin/_processed_/3/a/csm_shutterstock_795657547_803ce91d8c.jpg&quot;) no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #dff0f4;background-image: url(https://www.lazarski.pl/fileadmin/_processed_/3/a/csm_shutterstock_795657547_803ce91d8c.jpg);background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 10px groove #003c7d;padding-top: 100px;padding-bottom: 40px;"><!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;">
                    <tbody>
                      <tr>
                        <td valign="top" class="headerContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnTextBlockOuter">
                              <tr>
                                <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="600" style="width:600px;">
				<![endif]-->
                                  <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                      <tr>
                                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px;line-height: 200%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 18px;text-align: left;">
                                          <h1 style="text-align: center;color: #FFFFFF;background-color: #000000;opacity: 0.7;display: block;margin: 0;padding: 0;font-family: Georgia;font-size: 30px;font-style: normal;font-weight: normal;line-height: 125%;letter-spacing: normal;">
                                            <span style="font-size:32px; line-height: 40px">
                                              <span style="color:#FFFFFF; opacity:1.0">
                                                <strong>
                                                  <span style="font-family:helvetica neue,helvetica,arial,verdana,sans-serif">
                                                    <span>&nbsp;Si quieres ir rápido, ve solo. Si quieres llegar lejos, ve acompañado</span>
                                                  </span>
                                                </strong>
                                              </span>
                                            </span>
                                          </h1>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>

                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top" id="templateBody" style="background:#ffffff none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 50px;padding-bottom: 80px;">
        
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;">
                    <tbody>
                      <tr>
                        <td valign="top" class="bodyContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnTextBlockOuter">
                              <tr>
                                <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
             
                                  <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                      <tr>
                                        <td valign="top" class="mcnTextContent" style="padding: 0px 18px 9px;line-height: 100%;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #666666;font-family: Georgia;font-size: 16px;text-align: center;">
                                          <h2 style="display: block;margin: 0;padding: 0;color: #202020;font-family: Helvetica;font-size: 24px;font-style: normal;font-weight: bold;line-height: 150%;letter-spacing: normal;text-align: center;">
                                            <span style="color:#005180">
                                              <span style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">Usted ha sido invitado a participar como miembro de un evento, para más información sobre este presione el botón de abajo.</span>
                                            </span>
                                          </h2>
                                          <p style="line-height: 100%;margin: 10px 0;padding: 0;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #666666;font-family: Georgia;font-size: 16px;text-align: center;">
                                            <span style="font-size:12px">
                                              <span style="font-family:helvetica neue,helvetica,arial,verdana,sans-serif">Si aún no se encuentra registrado no se preocupe, en dos simples pasos podrá ser parte de la plataforma 
                                                más grande de eventos académicos en Latinoamerica.  
                                               </span>
                                            </span>
                                          </p>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnTextBlockOuter">
                              <tr>
                                <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;">
                            <tbody class="mcnDividerBlockOuter">
                              <tr>
                                <td class="mcnDividerBlockInner" style="min-width: 100%;padding: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                  <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody>
                                      <tr>
                                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                          <span></span>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnButtonBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnButtonBlockOuter">
                              <tr>
                                <td style="padding-top: 0;padding-right: 18px;padding-bottom: 18px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" valign="top" align="center" class="mcnButtonBlockInner">
                                  <table border="0" cellpadding="0" cellspacing="0" class="mcnButtonContentContainer" style="border-collapse: separate !important;border-radius: 3px;background-color: #003c7d;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody>
                                      <tr>
                                        <td align="center" valign="middle" class="mcnButtonContent" style="font-family: &quot;Helvetica Neue&quot;, Helvetica, Arial, Verdana, sans-serif;font-size: 16px;padding: 15px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                          <a class="mcnButton " title="Ir&nbsp;a&nbsp;Rendezvous." href="{}" target="_blank" style="font-weight: bold;letter-spacing: normal;line-height: 100%;text-align: center;text-decoration: none;color: #FFFFFF;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;display: block;">Ir&nbsp;a&nbsp;Rendezvous.</a>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;">
                            <tbody class="mcnDividerBlockOuter">
                              <tr>
                                <td class="mcnDividerBlockInner" style="min-width: 100%;padding: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                  <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody>
                                      <tr>
                                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                          <span></span>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
<!--            
                <td class="mcnDividerBlockInner" style="padding: 18px;">
                <hr class="mcnDividerContent" style="border-bottom-color:none; border-left-color:none; border-right-color:none; border-bottom-width:0; border-left-width:0; border-right-width:0; margin-top:0; margin-right:0; margin-bottom:0; margin-left:0;" />
-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                         
               
     
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnDividerBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;table-layout: fixed !important;">
                            <tbody class="mcnDividerBlockOuter">
                              <tr>
                                <td class="mcnDividerBlockInner" style="min-width: 100%;padding: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                  <table class="mcnDividerContent" border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-top: 2px solid #EAEAEA;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody>
                                      <tr>
                                        <td style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>

   
                        </td>
                      </tr>
                    </tbody>
                  </table>

                </td>
              </tr>
              <tr>
                <td align="center" valign="top" id="templateColumns" style="background:#ffffff none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #ffffff;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 0px;padding-bottom: 50px;">
                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;">
                    <tbody>
                      <tr>
                        <td valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody>
                              <tr>
                                <td valign="top" class="columnContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody class="mcnTextBlockOuter">
                                      <tr>
                                        <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                          <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                              <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;">
                                                  <img data-file-id="4913" height="50" src="https://gallery.mailchimp.com/ed526b2f15f645fc575e0db76/images/872cbeb9-07a2-49e1-8696-d6bd1ec7b0e1.png" style="border: 0px initial;width: 50px;height: 50px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="50">
                                                  <br>
                                                  <span style="font-size:12px">
                                                    <strong>
                                                      <span style="color:#005180">
                                                        <span style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">(855) 333-2222</span>
                                                      </span>
                                                    </strong>
                                                  </span>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody>
                              <tr>
                                <td valign="top" class="columnContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody class="mcnTextBlockOuter">
                                      <tr>
                                        <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                         
                                          <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                              <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;">
                                                  <a href="{}" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-weight: normal;text-decoration: underline;">
                                                    <img data-file-id="4917" height="50" src="https://gallery.mailchimp.com/ed526b2f15f645fc575e0db76/images/1626e444-a30b-406d-94fc-cfc3907b9b16.png" style="border: 0px;width: 50px;height: 50px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="50">
                                                  </a>
                                                  <br>
                                                  <span style="font-size:12px">
                                                    <a href="{}" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-weight: normal;text-decoration: underline;">
                                                      <strong>
                                                        <span style="color:#005180">
                                                          <span style="font-family:roboto,helvetica neue,helvetica,arial,sans-serif">rendezvous.com</span>
                                                        </span>
                                                      </strong>
                                                    </a>
                                                  </span>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
												<!--[if (gte mso 9)|(IE)]>
												</td>
												<td align="center" valign="top" width="200" style="width:200px;">
												<![endif]-->
                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="200" class="columnWrapper" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody>
                              <tr>
                                <td valign="top" class="columnContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody class="mcnTextBlockOuter">
                                      <tr>
                                        <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if mso]>
				<table align="left" border="0" cellspacing="0" cellpadding="0" width="100%" style="width:100%;">
				<tr>
				<![endif]-->
				<!--[if mso]>
				<td valign="top" width="200" style="width:200px;">
				<![endif]-->
                                          <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                            <tbody>
                                              <tr>
                                                <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #202020;font-family: Helvetica;font-size: 16px;line-height: 200%;text-align: center;">
                                                  <a href="mailto:rendezvoussystem@gmail.com" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-weight: normal;text-decoration: underline;">
                                                    <img data-file-id="4921" height="50" src="https://gallery.mailchimp.com/ed526b2f15f645fc575e0db76/images/d18fdca4-ceed-4c0d-afa5-d20024d8a229.png" style="border: 0px;width: 50px;height: 50px;margin: 0px;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" width="50">
                                                  </a>
                                                  <br>
                                                  <span style="font-size:12px">
                                                    <a href="mailto:rendezvoussystem@gmail.com" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #202020;font-weight: normal;text-decoration: underline;">
                                                      <font color="#005180" face="roboto, helvetica neue, helvetica, arial, sans-serif">
                                                        <strong>rendezvous@gmail.com</strong>
                                                      </font>
                                                    </a>
                                                  </span>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
												<!--[if (gte mso 9)|(IE)]>
												</td>
												</tr>
												</table>
												<![endif]-->
                        </td>
                      </tr>
                    </tbody>
                  </table>
                </td>
              </tr>
              <tr>
                <td align="center" valign="top" id="templateFooter" style="background:#003c7d none no-repeat center/cover;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;background-color: #003c7d;background-image: none;background-repeat: no-repeat;background-position: center;background-size: cover;border-top: 0;border-bottom: 0;padding-top: 60px;padding-bottom: 60px;"><!--[if (gte mso 9)|(IE)]>
									<table align="center" border="0" cellspacing="0" cellpadding="0" width="600" style="width:600px;">
									<tr>
									<td align="center" valign="top" width="600" style="width:600px;">
									<![endif]-->
                  <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%" class="templateContainer" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;max-width: 600px !important;">
                    <tbody>
                      <tr>
                        <td valign="top" class="footerContainer" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnFollowBlockOuter">
                              <tr>
                                <td align="center" valign="top" style="padding: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowBlockInner">
                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentContainer" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    <tbody>
                                      <tr>
                                        <td align="center" style="padding-left: 9px;padding-right: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                          <table border="0" cellpadding="0" cellspacing="0" width="100%" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContent">
                                            <tbody>
                                              <tr>
                                                <td align="center" valign="top" style="padding-top: 9px;padding-right: 9px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                  <table align="center" border="0" cellpadding="0" cellspacing="0" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                    <tbody>
                                                      <tr>
                                                        <td align="center" valign="top" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;"><!--[if mso]>
                                    <table align="center" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                    <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody>
                                                              <tr>
                                                                <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <tbody>
                                                                      <tr>
                                                                        <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                  <a href="http://www.facebook.com/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/light-facebook-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class="">
                                                                                  </a>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                        </td>
                                                                      </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody>
                                                              <tr>
                                                                <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <tbody>
                                                                      <tr>
                                                                        <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                  <a href="http://www.twitter.com/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/light-twitter-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class="">
                                                                                  </a>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                        </td>
                                                                      </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody>
                                                              <tr>
                                                                <td valign="top" style="padding-right: 10px;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <tbody>
                                                                      <tr>
                                                                        <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                  <a href="http://www.instagram.com/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/light-instagram-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class="">
                                                                                  </a>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                        </td>
                                                                      </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                        <!--[if mso]>
                                        <td align="center" valign="top">
                                        <![endif]-->
                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" style="display: inline;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                            <tbody>
                                                              <tr>
                                                                <td valign="top" style="padding-right: 0;padding-bottom: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" class="mcnFollowContentItemContainer">
                                                                  <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnFollowContentItem" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                    <tbody>
                                                                      <tr>
                                                                        <td align="left" valign="middle" style="padding-top: 5px;padding-right: 10px;padding-bottom: 5px;padding-left: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                          <table align="left" border="0" cellpadding="0" cellspacing="0" width="" style="border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                            <tbody>
                                                                              <tr>
                                                                                <td align="center" valign="middle" width="24" class="mcnFollowIconContent" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                  <a href="http://www.facebook.com/" target="_blank" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                                                                    <img src="https://cdn-images.mailchimp.com/icons/social-block-v2/light-link-48.png" style="display: block;border: 0;height: auto;outline: none;text-decoration: none;-ms-interpolation-mode: bicubic;" height="24" width="24" class="">
                                                                                  </a>
                                                                                </td>
                                                                              </tr>
                                                                            </tbody>
                                                                          </table>
                                                                        </td>
                                                                      </tr>
                                                                    </tbody>
                                                                  </table>
                                                                </td>
                                                              </tr>
                                                            </tbody>
                                                          </table>
                                        <!--[if mso]>
                                        </td>
                                        <![endif]-->
                                    <!--[if mso]>
                                    </tr>
                                    </table>
                                    <![endif]-->
                                                        </td>
                                                      </tr>
                                                    </tbody>
                                                  </table>
                                                </td>
                                              </tr>
                                            </tbody>
                                          </table>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table border="0" cellpadding="0" cellspacing="0" width="100%" class="mcnTextBlock" style="min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                            <tbody class="mcnTextBlockOuter">
                              <tr>
                                <td valign="top" class="mcnTextBlockInner" style="padding-top: 9px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;">
                                    
                                  <table align="left" border="0" cellpadding="0" cellspacing="0" style="max-width: 100%;min-width: 100%;border-collapse: collapse;mso-table-lspace: 0pt;mso-table-rspace: 0pt;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;" width="100%" class="mcnTextContentContainer">
                                    <tbody>
                                      <tr>
                                        <td valign="top" class="mcnTextContent" style="padding-top: 0;padding-right: 18px;padding-bottom: 9px;padding-left: 18px;mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;word-break: break-word;color: #FFFFFF;font-family: Helvetica;font-size: 12px;line-height: 150%;text-align: center;">© 2019-2019 Rendezvous®, Todos los Derechos Reservados.
                                          <br>
                                          <div style="text-align: center;">
                                            <a href="*|UNSUB|*" style="mso-line-height-rule: exactly;-ms-text-size-adjust: 100%;-webkit-text-size-adjust: 100%;color: #FFFFFF;font-weight: normal;text-decoration: underline;">Terminos de uso</a>
                                          </div>
                                        </td>
                                      </tr>
                                    </tbody>
                                  </table>
				<!--[if mso]>
				</td>
				<![endif]-->
				<!--[if mso]>
				</tr>
				</table>
				<![endif]-->
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </tbody>
                  </table>
									<!--[if (gte mso 9)|(IE)]>
									</td>
									</tr>
									</table>
									<![endif]-->
                </td>
              </tr>
            </tbody>
          </table>
                        <!-- // END TEMPLATE -->
        </td>
      </tr>
    </tbody>
  </table>
</center>
<br>""".format(link, link, link)
    all_guests=[]
    print(guest_list)
    for guest in guest_list:
        all_guests.append(guest['email'])
    
    all_guests = list(set(all_guests))
    # CONFIGURACION DEL MENSAJE
    msg = MIMEMultipart('alternative')
    msg['Subject'] = '[RENDEZVOUS] Invitacion a participar'
    msg.attach(MIMEText(messageHTML, 'html'))
    
    with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
        smtp.login(SYSTEM_EMAIL, SYSTEM_PASSWORD)
        smtp.sendmail(SYSTEM_EMAIL, all_guests, msg.as_string())


class UpdateEvent(Resource):
    def post(self):
        data = parser.parse_args()
        now = datetime.now()
        # se busca el type dentro de la base de datos
        current_type = TypeController.query_type_by_name(data['type'])

        # validacion de evento
        event =  EventController.query_event_by_id(data['event_id'])
        event_id = event.id
        user = UserController.query_user_by_username(data['username'])
        user_id =user.id

        if data['picture']: 
            picture = data['picture']
            if picture and allowed_file(picture.filename):
                if event.picture: delete_file_of_s3(event.picture)
                picture_filename = secure_filename(picture.filename)
                picture_url = upload_image_to_s3(picture, str(picture_filename))
            else:
                return jsonify(status="error", message="No se subió la foto")
        else: 
            picture_url = " "
            
        if data['programme']: 
            # una vez validad la data se guarda en los path correspondientes
            programme = data['programme']
            if programme and allowed_file(programme.filename):
                if event.programme:delete_file_of_s3(event.programme)
                programme_filename = secure_filename(programme.filename)
                programme_url = upload_file_to_s3(programme, programme_filename)
            else:
                return jsonify(status="error", message="No se subió el paper")
        else: 
            programme_url= " "


        if data['name'] : event.name= data['name'] 
        if data['description'] : event.description=data['description']
        if data['vacancies'] : event.vacancies=data['vacancies']
        if data['location'] : event.location=data['location']
        if data['start_date'] : event.start_date= (process_date(data['start_date']) if data['start_date'] else None)
        if data['end_date'] : event.end_date = (process_date(data['end_date']) if data['end_date'] else None)
        if data['dayCfP'] : event.limit_date_call_for_papers = (process_date(data['dayCfP']) if data['dayCfP'] else None)
        if data['type'] : event.type=current_type
        if data['programme'] : event.programme= programme_url
        if data['picture'] : event.picture= picture_url
        if data['amount'] : event.amount=data['amount']
        if data['status'] : event.amount=data['status']
        event.remaining_vacancies=int(data['vacancies'])-(int(event.vacancies)-int(event.remaining_vacancies))
        event.last_modification_date=now
        event.last_modified_user_id=user_id

        #Actualizar keywords

        kws = KeywordController.query_keywords_by_event(event.id)
        keywords = [kw.name for kw in kws]
        new = []

        json_keywords_data = json.loads(data['keywords'])
        for keyword in json_keywords_data:
            if not keyword['name'] in keywords:
                new.append(keyword['name'])
            else:
                keywords.remove(keyword['name'])

        #lo que está en new se agrega a la relación
        for keyword in new:
            new_keyword = KeywordController.query_keyword_by_name(keyword)
            if not new_keyword:
                new_keyword = KeywordModel(name=keyword,creation_date=now,creation_user_id=user_id)
                notify = KeywordController.insert_keyword(new_keyword)
                if notify.status == "error":
                    return jsonify(status="error", message="Something wrong in Keyword Registration")
            # se agrega en la tabla intermedia
            new_keyword.event_keyword.append(event)
            KeywordController.insert_keyword(new_keyword)

        #lo restante en keywords se elimina, ya que quiere decir que ya no estan en las seleccionadas por el usuario
        for keyword in keywords:
            keyword_model = KeywordController.query_keyword_by_name(keyword)
            #se elimina de la tabla intermedia
            keyword_model.event_keyword.remove(event)
            KeywordController.insert_keyword(keyword_model)

        members = PersonController.query_members_by_event(event_id)
        delete = [{"email":m.email,"role":m.role} for m in members]
        json_members_data = json.loads(data['members'])
        #json_members_data = [{"email":"gzarate@pucp.pe","role":"Miembro del comité organizador"},
        #                    {"email":"jsoto@gmail.com","role":"Postulante"}]
        new = []
        for member in json_members_data:
            encontrado = False
            for m in delete:
                if member['email']==m["email"] and member['role']==m["role"]:
                    delete.remove(m)
                    encontrado=True
                    break
            #no lo encontró
            if not encontrado:new.append(member)

        #primero se van a insertar los nuevos miembros
        for member in new:
            # primero se busca a la persona relacionada con el e-mail
            person_aux = PersonController.query_person_by_email(
                member['email'])
            if not person_aux:
                return jsonify(status="error", message="Something wrong in Member Registration")
            #si se encuentra 
            new_user = UserController.query_user_by_person_id(person_aux.id)
            # VERIFICA EXISTENCIA
            if not new_user: 
                return jsonify(status="error", message="Something wrong in Member Registration")

            #primero se verifica que no este ya creada la relacion event x user
            new_event_user = EventUserController.query_by_event_id_and_user_id(
                event_id=event_id, user_id=new_user.id)
            #si no está se crea
            if not new_event_user:
                new_event_user = EventUserModel(
                    user=new_user,
                    event=event
                )

            role = RoleController.query_role_by_name(member['role'])
            #se agrega la relacion entre role y  event_user
            new_event_user.event_user_role.append(role)
            #finalmente se agrega a eventxuser
            EventUserController.insert_event_user(new_event_user)
        print(data['link'])
        if new: send_mail(new,data['link'])

        #se eliminan los miembros que ya no están
        for member in delete:
            person_aux = PersonController.query_person_by_email(
                member['email'])
            new_user = UserController.query_user_by_person_id(person_aux.id)
            new_event_user = EventUserController.query_by_event_id_and_user_id(
                event_id=event_id, user_id=new_user.id)
            role = RoleController.query_role_by_name(member['role'])
            new_event_user.event_user_role.remove(role)
            EventUserController.insert_event_user(new_event_user)


        notify = EventController.insert_event(event)
        if notify.status == "error":
            return jsonify(status="error", message="Something wrong in Event Registration")

        write_to_log(now.strftime("%d-%m-%Y (%H:%M:%S): ") + "Se modificó el evento" + data['event_id'] + "\n", 'log.txt')
        return jsonify(status="ok", message="Event {} updated satisfactorily".format(data['name']))
