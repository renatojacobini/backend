from flask_restful import Resource, reqparse
from application.controller.event_controller import EventController
from application.controller.type_controller import TypeController 
from application.controller.keyword_controller import KeywordController 
from application.controller.person_controller import PersonController 
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('event_id', help='Not Blank', required=True)

class ViewEventInformation(Resource): 
	def post(self):
		data = parser.parse_args()
		event =  EventController.query_event_by_id(data['event_id'])
		event_type = TypeController.query_by_id(event.type_id)
		members_json=[]
		members = PersonController.query_members_by_event(data['event_id'])
		for member in members:
			members_json.append( {"email":member.email,
								  "role":member.role
								 })
		keywords_json=[]
		keywords = KeywordController.query_keywords_by_event(data['event_id'])
		for keyword in keywords:
			keywords_json.append({"name":keyword.name})

		dicc = {
		    "name":event.name,
			"description":event.description,
			"vacancies":event.vacancies,
			"location":event.location,
			"start_date":event.start_date,
			"end_date":event.end_date,
			"type":event_type.name,
			"programme":event.programme,
			"picture":event.picture,
			"amount":event.amount,
			"status":event.status,
			"members":members_json,
			"keywords" :keywords_json,
			"dayCfP" : event.limit_date_call_for_papers
		}

		return jsonify(
			event=dicc
			)
