from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController

from application.models.person_model import PersonModel
from application.models.user_model import UserModel

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)

class RequestOrganizerPermission(Resource): 
    def post(self):
        data = parser.parse_args()
        current_user = UserController.query_user_by_username(
                data['username'])
        current_user.request_organizer = 1
        notify = UserController.update_request_organizer(current_user.username)
        return jsonify(status="ok", message="El usuario {} pidio permiso de organizador correctamente".format(data['username']))
