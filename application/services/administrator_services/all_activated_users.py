from flask_restful import reqparse, Resource
from flask import json, jsonify
from application.resources.time_resources import parse_str_to_date
from application.controller.session_controller import SessionController

parser = reqparse.RequestParser()
parser.add_argument('date_start', help='Not Blank', required=True)
parser.add_argument('date_end', help='Not Blank', required=True)


class AllActivatedSessions(Resource):
    def post(self):
        data = parser.parse_args()
        date_start = parse_str_to_date(data['date_start'])
        date_end = parse_str_to_date(data['date_end'])

        sessions = SessionController.query_all_sessions_in_range(
            date_start, date_end)
        sessions_json = [{"n_sessions": 0} for i in range(12)]

        if sessions:
            for session in sessions:
                sessions_json[session.date_session.month -
                              1]["n_sessions"] += 1

        return jsonify(sessions=sessions_json)
