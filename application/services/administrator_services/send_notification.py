from flask_restful import Resource, reqparse
from flask import jsonify, request
# emaill!
import smtplib
import os 
import json 
from email.message import EmailMessage 

parser = reqparse.RequestParser()
parser.add_argument('guests', help='Not Blank', required=True, action="append")

SYSTEM_EMAIL = os.environ.get('SYSTEM_EMAIL')
SYSTEM_PASSWORD = os.environ.get('SYSTEM_PASSWORD')



class SendNotification(Resource):
    def post(self):
        data = parser.parse_args()
        all_guests = []
        for guest in data['guests']:
            guests_json = json.loads(guest.replace("'", "\""))
            print(guests_json['email'])
            #los datos ya vienen filtrados, estos correos ya existen!
            all_guests.append(guests_json['email'])
        
        # CONFIGURACION DEL MENSAJE
        msg = EmailMessage()
        msg['Subject'] = '[IMPORTANTE] Invitacion a participar'
        msg['From'] = SYSTEM_EMAIL
        msg['To'] = all_guests
        msg.set_content('Usted ha sido invitado a participar')
        
        with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp: 
            smtp.login(SYSTEM_EMAIL, SYSTEM_PASSWORD) 
            smtp.send_message(msg)
 
        return jsonify(status="ok", message="se han enviado todos los correos")
