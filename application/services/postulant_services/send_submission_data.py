from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController
from application.controller.submission_controller import SubmissionController
from application.controller.keyword_controller import KeywordController
from application.controller.field_controller import FieldController

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('id_event', help='Not Blank', required=True)
parser.add_argument('id_phase', help='Not Blank', required=True)

class SendSubmissionData(Resource): 
    def post(self):
        data = parser.parse_args()
        current_user = UserController.query_user_by_username(
                data['username'])
        #current_person = PersonController.query_by_id(current_user.fk_person_id)
        if not current_user:
            return jsonify(status="error", message='Username {} not exist'.format(data['username']))
        event = EventController.query_event_by_id(data['id_event'])
        phase = PhaseController.query_phase_by_id(data['id_phase'])
        submission = SubmissionController.query_by_user_and_phase(current_user.id,phase.id)
        #submissions = SubmissionController.query_by_user_and_phase(current_user.id,phase.id)
        #for i in submissions:
        #    if i.phase_id == phase.id:
        #        submission = i
        #        break
        json_keywords = []
        json_keywords_choose = []
        json_authors = []
        json_fields = []
        json_keywords_data = KeywordController.query_keywords_by_submission(submission.id)
        for keyword in json_keywords_data:
            json_keywords_choose.append({"name":keyword.name})
        json_keywords_data = KeywordController.query_keywords_by_event(event.id)
        for keyword in json_keywords_data:
            json_keywords.append({"name":keyword.name})
        json_authors_data = PersonController.query_by_submission(submission.id)
        for id_author in json_authors_data:
            author = PersonController.query_by_id(id_author[1])
            json_authors.append( {"name":author.first_name,
								  "last_name":author.last_name,
                                  "email":author.email,
                                  "affiliation":author.affiliation
								 })
        json_fields_data = FieldController.query_fields_by_id(phase.id)
        for field in json_fields_data:
            json_fields.append({
                "id_field":field.id,
                "name_field":field.name,
                "content_field":field.content
            })
        return{
            "title_event":event.name,
            "startDate":phase.start_date.strftime("%d-%m-%Y"),
            "endDate":phase.end_date.strftime("%d-%m-%Y"),
            "name":submission.name,
            "document":submission.document,
            "summary":submission.summary,
            "keywords_choose":json_keywords_choose,
            "keywords":json_keywords,
            "authors":json_authors,
            "fields":json_fields,
            "id_submission":submission.id
        }
