from flask_restful import Resource, reqparse
from application.resources.aws_s3 import delete_file_of_s3

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('filename', help='Not Blank', required=True)

class DeleteFile(Resource):
    def post(self):
        data = parser.parse_args()
        filename = data['filename']
        response = delete_file_of_s3(filename)
        print(response)
        return jsonify(status="ok", message="se ha eliminado el archivo {}".format(filename))
