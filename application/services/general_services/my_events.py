from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.event_controller import EventController
from application.controller.type_controller import TypeController

from flask import jsonify, json
import os 
from pathlib import Path

#FILES MANAGE
application_folder = Path.cwd().joinpath('application')
images_folder = application_folder.joinpath('files', 'images')

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)

class MyEvents(Resource): 
    def post(self):
        data = parser.parse_args()
        if '@' in str(data['username']):
            current_person = PersonController.query_person_by_email(
                data['username'])
            if not current_person:
                return jsonify(status="error", message="Person with email {} not exist".format(data['username']))
            #se busca el usuario de la persona
            current_user = UserController.query_user_by_person_id(
                current_person.id)
        else:
            current_user = UserController.query_user_by_username(
                data['username'])
            if not current_user:
                return jsonify(status="error", message='Username {} not exist'.format(data['username']))
        # se obtienen los eventos del usuario
        events_user = EventUserController.query_events_by_user_id(current_user.id)
        # ahora para cada evento se obtienen los roles 
        event_json = []
        for event_user in events_user:
            if event_user.EventUserModel.status == 1: 
                roles = EventUserRoleController.query_roles_by_event_user_id(
                    user_id=event_user.EventUserModel.user_id, event_id=event_user.EventUserModel.event_id)
                if roles:
                    role_json = []
                    #primero se obtiene el json de todos los roles 
                    for role in roles:
                        role_json.append({"name":role.name}) 
                    #se busca la imagen de cada evento 
                    picture_url = EventController.query_event_by_id(event_user.EventUserModel.event_id).picture
                    #finalmente se agrega al evento
                    event_json.append({
                        "id_event":event_user.EventModel.id,
                        "title":event_user.EventModel.name,
                        "type":event_user.EventModel.type.name ,
                        # "type":type_str.name
                        "roles":role_json,
                        "startDate": event_user.EventModel.start_date.strftime("%d/%m/%Y"),
                        "endDate": event_user.EventModel.end_date.strftime("%d/%m/%Y"),
                        "location":event_user.EventModel.location,
                        "image": str(picture_url) if picture_url else None
                    })

        return jsonify(events=event_json)
