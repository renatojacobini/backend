from flask_restful import Resource
from application.controller.type_controller import TypeController

class AllTypes(Resource): 
    def post(self):
        return TypeController.query_all_types()
