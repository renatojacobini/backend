from flask_restful import Resource, reqparse
from application.controller.submission_controller import SubmissionController 
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('phase_id', help='Not Blank', required=True)

class CountSubmissionsByPhase(Resource): 
	def post(self):
		data = parser.parse_args()
		count = SubmissionController.count_submissions_by_phase(data['phase_id'])

		return jsonify(total_submissions= count)
