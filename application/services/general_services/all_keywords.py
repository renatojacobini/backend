from flask_restful import Resource, reqparse 
from application.controller.keyword_controller import KeywordController

class AllKeywords(Resource): 
    def post(self): 
        return KeywordController.query_all_keywords()
