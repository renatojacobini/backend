from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.event_user_controller import EventUserController
from application.controller.event_controller import EventController
from application.controller.event_user_role_controller import EventUserRoleController
from application.controller.phase_controller import PhaseController

from application.models.person_model import PersonModel
from application.models.user_model import UserModel
from datetime import datetime

from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('username_new', help='Not Blank', required=True)
parser.add_argument('first_name', help='Not Blank', required=True)
parser.add_argument('last_name', help='Not Blank', required=True)
parser.add_argument('email', help='Not Blank', required=True)

class UpdateDataUser(Resource): 
    def post(self):
        data = parser.parse_args()
        now = datetime.now()
        current_user = UserController.query_user_by_username(
                data['username'])
        current_person = PersonController.query_by_id(current_user.fk_person_id)
        if not current_user:
            return jsonify(status="error", message='Username {} not exist'.format(data['username']))
        current_user.username = data['username_new']
        current_person.first_name = data['first_name']
        current_person.last_name = data['last_name']
        current_person.email = data['email']
        current_person.last_modification_date=now
        current_person.last_modified_user_id=current_user.id
        PersonController.insert_person(current_person)

        notify = UserController.update_content(data['username'],current_user.username,current_person.first_name,current_person.last_name)
        return jsonify(status="ok", message="Username {} updated satisfactorily".format(data['username_new']))
