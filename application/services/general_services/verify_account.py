from flask_restful import Resource, reqparse
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from flask import jsonify, json

parser = reqparse.RequestParser()
parser.add_argument('first_name', help='Not Blank', required=True)
parser.add_argument('last_name', help='Not Blank', required=True)
parser.add_argument('email', help='Not Blank', required=True)

class VerifyAccount(Resource): 
    def post(self):
        data = parser.parse_args()
        first_name = data['first_name']
        last_name = data['last_name']
        email = data['email']
        person = PersonController.query_person_by_email(email)
        if not person:
            return jsonify(status="ok", message="La persona no tiene una cuenta registrada en el sistema")
        else:
            if (person.first_name.lower() == first_name.lower() and person.last_name.lower() == last_name.lower()):
                return jsonify(status="ok", message="La persona sí tiene una cuenta registrada en el sistema")
            else:
                return jsonify(status="error", message="La persona intenta utilizar un correo que ya existe en el sistema")
