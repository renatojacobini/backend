from flask_restful import Resource, reqparse
from application.models.user_model import UserModel
from flask import jsonify
from application.controller.person_controller import PersonController
from application.controller.user_controller import UserController
from application.controller.permission_user_controller import PermissionUserController
from application.controller.session_controller import SessionController
from application.models.session_model import SessionModel
from datetime import datetime
from google.oauth2 import id_token
from google.auth.transport import requests
import os
GOOGLE_CLIENT_ID = os.environ.get("GOOGLE_CLIENT_ID")

parser = reqparse.RequestParser()
parser.add_argument('username', help='Not Blank', required=True)
parser.add_argument('token', help='Not Blank', required=True)


class GoogleLogin(Resource):
    def post(self):
        data = parser.parse_args()
        if '@' in str(data['username']):
            current_person = PersonController.query_person_by_email(
                data['username'])
            if not current_person:
                return jsonify(status="error", message="Person with email {} not exist".format(data['username']))
            #se busca el usuario de la persona
            current_user = UserController.query_user_by_person_id(
                current_person.id)
        else:
            current_user = UserController.query_user_by_username(
                data['username'])
            if not current_user:
                return jsonify(status="error", message='Username {} not exist'.format(data['username']))
        
        # se procesa el token
        try:
            idinfo = id_token.verify_oauth2_token(
                data['token'], requests.Request(), GOOGLE_CLIENT_ID)
            if idinfo['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                print('===> Wrong issuer.')
            password = idinfo['sub']
        except:
            # Invalid token
            return jsonify(status="error", message="Invalid Token")

        if UserModel.verify_hash(password, current_user.password):
            new_session = SessionModel(date_session=datetime.now())
            SessionController.insert_session(new_session)

            # si es el usuario correcto se verifican sus permisos
            permissions_user = PermissionUserController.query_permissions_by_user_id(
                current_user.id)

            isOrganizer = False
            isAdministrator = False
            if permissions_user:
                for permission_user in permissions_user:
                    if permission_user.name == "Organizer":
                        isOrganizer = True
                    if permission_user.name == "Administrator":
                        isAdministrator = True

            return jsonify(status="ok",
                           message='Loggued as {}'.format(
                               current_user.username),
                           isAdmin=isAdministrator,
                           isOrganizer=isOrganizer)
        else:
            return jsonify(status="error", message='Incorrect password')
